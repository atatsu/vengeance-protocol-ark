.PHONY: test purge-pyc

test:
	python -m pytest -v

purge-pyc:
	find . -name "*.pyc" -delete;

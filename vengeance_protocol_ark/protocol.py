from vengeance.protocol import Protocol


class ARKSurvivalProtocol(Protocol):
	"""
	Vengeance protocol allowing interaction with an ARK Survival.
	"""
